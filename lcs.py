# Tanner Armstrong
# Computer Science 2500
# Longest Common Subsequence Algorithm


# Pre-condition: x and y are strings of length > 0
# Post-condition: c is a two dimensional array of integers representing the length of all possible subsequences
#                 b is a two dimensional array of strings representing how to walk through the c array to find the longest common sub sequence
def lcs_length(x, y):
    m = len(x)
    n = len(y)

    b = [[x for x in range(1, m+1)] for y in range(1, n+1)]
    c = [[x for x in range(0, m+1)] for y in range(0, n+1)]

    # initialize the values in the first row and first col of c to be 0
    for i in range(1, n):
        c[i][0] = 0
    for j in range(0, m+1):
        c[0][j] = 0
    
    # Loop Invariant: for every i and j, c[i][j] <= c[i+1][j+1]
    for i in range(1, n+1):
        for j in range(1, m+1):

            if x[j-1] == y[i-1]:
                c[i][j] = c[i-1][j-1] + 1
                b[i-1][j-1] = "diag"

            elif c[i-1][j] >= c[i][j-1]:
                c[i][j] = c[i-1][j]
                b[i-1][j-1] = "up"

            else:
                c[i][j] = c[i][j-1]
                b[i-1][j-1] = "sideways"

    return c, b

# Pre-condition: b is the array returned from the lcs_length function
#                Y is the second string passed to the lcs_length function
#                i is the length of string Y and j is the length of string X in the lcs_length function
# Post-condition: the longest common subsequence of the two strings are printed to std output
def print_lcs(b, Y, i, j):

    if i == 0 or j == 0:
        return
    
    if b[i-1][j-1] == "diag":
        print_lcs(b, Y, i-1, j-1)
        print(str(Y[i-1]))
    
    elif b[i-1][j-1] == "up":
        print_lcs(b, Y, i-1, j)
    
    else:
        print_lcs(b, Y, i, j-1)
    

if __name__ == "__main__":
    seqx = "ACCGGTCGAGTGCGCGGAAGCCGGCCGAA"
    seqy = "GTCGTTCGGAATGCCGTTGCTCTGTAAA"

    print("Sequence x - {}\nSequence y - {}\n".format(seqx, seqy))
    c, b = lcs_length(seqx, seqy)

    print("The longest common subsequence of the strings is...")
    print_lcs(b, seqy, len(seqy), len(seqx))